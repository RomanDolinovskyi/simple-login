<?php
    require_once 'dbconfig.php';
    require_once 'query.php';


    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
    header('Access-Control-Allow-Headers: Content-Type, Authorization');
    header('Access-Control-Allow-Credentials: true');

    header('Content-type: application-json');   

    $method = $_SERVER['REQUEST_METHOD'];
    $url = explode('/', $_GET['q']);
    
    $path = $url[1];
    $params = $url[2];

    if($method === 'GET'){
        switch ($path){
            case "users":
                if(isset($params)){
                    getUser($connect, $params);
                }
            }      
    } else if($method === 'POST'){
        switch ($path){
            case "register":
                addUser($connect, $_POST);
                break;
            case "login":
                getUser($connect, $_POST);
        }
    }else if($method === 'DELETE'){
        switch ($path){
            case "delete":
                if($params){
                    deleteUser($connect, $params);
                }
        }
    };
    

