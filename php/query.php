<?php

function getUser($connect, $id){
    if(getallheaders()['Authorization'] === $id){
        $user = mysqli_query($connect, "SELECT * FROM users WHERE id = $id;");

        if(mysqli_num_rows($user) === 0 || !$user){
            http_response_code(404);
            $res = [
                'status' => false,
                'message' => mysqli_error($connect)
            ];
            echo json_encode($res);
        }else{
            http_response_code(200);
            $user = mysqli_fetch_assoc($user);
            echo json_encode($user);
        }

    } else if (isset($id['user_email']) && isset($id['user_password'])){
        $user_email = $id['user_email'];
        $user_password = $id['user_password'];
        $user = mysqli_query($connect, "SELECT * FROM users WHERE user_password = '$user_password' AND user_email = '$user_email';");
        if(mysqli_num_rows($user) === 0 || !$user){
            http_response_code(400);
            $res = [
                'status' => false,
                'message' => 'no such user'
            ];
            echo json_encode($res);
        }else{
            $user = mysqli_fetch_assoc($user);
            echo json_encode($user);
        }

    } else {
        http_response_code(404);
        $res = [
            'status' => false,
            'message' => "Unauthorized"
        ];
        echo json_encode($res);
    }
}

function addUser($connect, $data){
    $user_login = $data['user_login'];
    $user_password = $data['user_password'];
    $user_adress = $data['user_adress'];
    $user_email = $data['user_email'];
    $user_phone = $data['user_phone'];
    $user_gender = $data['user_gender'];
    
    $user = mysqli_query($connect, "INSERT INTO users (user_login, user_email, user_password, user_adress, user_phone, user_gender) 
    VALUES ('$user_login', '$user_email', '$user_password', '$user_adress', '$user_phone', '$user_gender');");

    if(mysqli_error($connect)){
        http_response_code(400);
        $res = [
            'status' => false,
            'message' => mysqli_error($connect)
        ];
        echo json_encode($res);
    }else{
        http_response_code(201);
        $user = mysqli_fetch_assoc(mysqli_query($connect, "SELECT * FROM users WHERE user_password = '$user_password' AND user_email = '$user_email';"));
        echo json_encode($user);
    }
}

function deleteUser($connect, $id){
    if(getallheaders()['Authorization'] === $id){
        mysqli_query($connect, "DELETE FROM users WHERE id = '$id';");
        if(mysqli_error($connect)){
            http_response_code(400);
            $res = [
                'status' => false,
                'message' => mysqli_error($connect)
            ];
        }else{
            http_response_code(201);
            $res = [
                'status' => true,
                'message' => 'Deleted successfully'
            ];
        }
    }else{
        http_response_code(401);
        $res = [
            'status' => false,
            'message' => 'Unauthorized'
        ]; 
    }
    echo json_encode($res);
}
