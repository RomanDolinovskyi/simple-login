USE mysql_db;

CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    user_login VARCHAR(255) NOT NULL,
    user_email VARCHAR(255) NOT NULL UNIQUE,
    user_password VARCHAR(255) NOT NULL,
    user_adress VARCHAR(255) NOT NULL,
    user_phone VARCHAR(20) NOT NULL UNIQUE,
    user_gender CHAR(1) NOT NULL
)